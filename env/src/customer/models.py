# from django.db import models
# from django.contrib.auth.models import AbstractUser
# from django.utils import timezone



# # Create your models here.
# class Customer(AbstractUser):
# 	username = models.CharField(max_length=200)
# 	email = models.EmailField(blank=False,unique=True)
# 	date_joined = models.DateTimeField(('date_joined'),default=timezone.now)
# 	date_updated = models.DateField(default=timezone.now)
# 	is_active = models.BooleanField(default=True)
# 	is_admin = models.BooleanField(default=False)
# 	is_staff = models.BooleanField(default=False)
# 	stripe_id = models.CharField(max_length=255)
# 	plan = models.CharField(max_length=100)

# 	USERNAME_FIELD = 'email'
# 	REQUIRED_FIELDS = ['username']
# 	#objects = CustomUserManager()

# 	class Meta:
# 		verbose_name = ('user')
# 		verbose_name_plural = ('users')

# 	def get_absolute_url(url):
# 		return "users/%s/" % urlquote(self.email)

# 	def get_short_name(self):
# 		return self.first_name

