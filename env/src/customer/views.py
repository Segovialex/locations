from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators import csrf
# from . forms import CustomUserForm
from django.conf import settings
import datetime
import stripe

stripe.api_key = settings.STRIPE_SECRET


@login_required
def checkout(request):
	subscription = stripe.Subscription.create(
  customer='cus_4fdAW5ftNQow1a',
  items=[{'plan': 'plan_CBb6IXqvTLXp3f'}],
  billing_cycle_anchor=1577995980,
)
	return render(request,'checkout.html')
