"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from emails.views import email_list_signup

urlpatterns = [
    path('admin/', admin.site.urls),
   # path('newsletter/', include('newsletter.urls')),
    path('email-signup/', email_list_signup, name='email-list-signup'),
    path('', include('shop.urls')),
    path('general/', include('general.urls')),
    path('accounts/', include('allauth.urls')),
    path('subscription/', include('customer.urls')),
    path('blog/', include('blog.urls')),
    path('about/', include('contact.urls')),








    
]
